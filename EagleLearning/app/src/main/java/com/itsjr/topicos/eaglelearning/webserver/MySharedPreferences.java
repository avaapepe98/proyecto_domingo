package com.itsjr.topicos.eaglelearning.webserver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;



        import java.util.Set;
        @SuppressLint("NewApi")
public class MySharedPreferences {
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

           /**
      * Constructor MySharedPreferences
      *
      * @param context
      */
            public MySharedPreferences(Context context) {
                mSharedPreferences = context.getSharedPreferences(
                                Config.NAME_SHARE_PREFERENCE, Context.MODE_PRIVATE);
            }

            public boolean clearInformation() {
                try {
                        mEditor.clear();
                        mEditor.commit();
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
            }

            /**
      * putString Método que sirve para guardar un String, se envÃ­a la llave que
      * se usarÃ¡ para guardarlo, y el valor
      *
      * @param key
      *            - {@link String}.
      * @param defValue
      *            - {@link String}.
      */
            public void putString(String key, String defValue) {
                mEditor = mSharedPreferences.edit();
                mEditor.putString(key, defValue);
                mEditor.commit();
            }

            /**
      * getString Método que sirve para obtener un String, se envÃ­a la llave que
      * se usarÃ¡ para guardarlo, y el valor
      *
      * @param key
      *            - {@link String}.
      * @param defValue
      *            - {@link String}.
      * @return - {@link String}.
      */
            public String getString(String key, String defValue) {
                return mSharedPreferences.getString(key, defValue);
            }

            /**
 -     * putInt MÃ©todo que sirve para guardar un int, se envÃ­a la llave que se
 -     * usarÃ¡ para guardarlo, y el valor
 -     *
 -     * @param key
 -     *            - {@link String}.
 -     * @param defValue
 -     *            - {@link Integer}.
 -     */
            public void putInt(String key, int defValue) {
                mEditor = mSharedPreferences.edit();
                mEditor.putInt(key, defValue);
                mEditor.commit();
            }

            /**
 -     * getInt MÃ©todo que sirve para obtener un int, se envÃ­a la llave que se
 -     * usarÃ¡ para guardarlo, y el valor
 -     *
 -     * @param key
 -     *            - {@link String}.
 -     * @param defValue
 -     *            - {@link Integer}.
 -     * @return - {@link Integer}.
 -     */
            public int getInt(String key, int defValue) {
                return mSharedPreferences.getInt(key, defValue);
            }

            /**
 -     * putBoolean MÃ©todo que sirve para guardar un boolean, se envÃ­a la llave
 -     * que se usarÃ¡ para guardarlo, y el valor
 -     *
 -     * @param key
 -     *            - {@link String}.
 -     * @param defValue
 -     *            - {@link Boolean}.
 -     */
            public void putBoolean(String key, boolean defValue) {
                mEditor = mSharedPreferences.edit();
                mEditor.putBoolean(key, defValue);
                mEditor.commit();
            }

            /**
 -     * getBoolean MÃ©todo que sirve para obtener un boolean, se envÃ­a la llave
 -     * que se usarÃ¡ para guardarlo, y el valor
 -     *
 -     * @param key
 -     *            - {@link String}.
 -     * @param defValue
 -     *            - {@link Boolean}.
 -     * @return - {@link Boolean}.
 -     */
            public boolean getBoolean(String key, boolean defValue) {
                return mSharedPreferences.getBoolean(key, defValue);
            }

            /**
 -     * putArrayString MÃ©todo que sirve para guardar un Set<String>, se envÃ­a la
 -     * llave que se usarÃ¡ para guardarlo, y el valor
 -     *
 -     * @param key
 -     *            - {@link String}.
 -     * @param defValue
 -     *            - {@link java.util.Set} de {@link String}.
 -     */
            public void putArrayString(String key, Set<String> defValue) {
                mEditor = mSharedPreferences.edit();
                mEditor.putStringSet(key, defValue);
                mEditor.commit();
            }

            /**
 -     * getArrayString MÃ©todo que sirve para obtener un Set<String>, se envÃ­a la
 -     * llave que se usarÃ¡ para guardarlo, y el valor
 -     *
 -     * @param key
 -     *            - {@link String}.
 -     * @param defValue
 -     *            - {@link java.util.Set} de {@link String}.
 -     * @return - {@link java.util.Set} de {@link String}.
 -     */
            public Set<String> getArrayString(String key, Set<String> defValue) {
                return mSharedPreferences.getStringSet(key, defValue);
            }

            /**
 -     * putLong MÃ©todo que sirve para guardar un Long, se envÃ­a la llave que se
 -     * usarÃ¡ para guardarlo, y el valor
 -     *
 -     * @param key
 -     *            - {@link String}.
 -     * @param defValue
 -     *            - {@link Long}.
 -     */
            public void putLong(String key, Long defValue) {
                mEditor = mSharedPreferences.edit();
                mEditor.putLong(key, defValue);
                mEditor.commit();
            }

            /**
 -     * getLong MÃ©todo que sirve para obtener un Long, se envÃ­a la llave que se
 -     * usarÃ¡ para guardarlo, y el valor
 -     *
 -     * @param key
 -     *            - {@link String}.
 -     * @param defValue
 -     *            - {@link Long}.
 -     * @return - {@link Long}.
 -     */
            public Long getLong(String key, Long defValue) {
                return mSharedPreferences.getLong(key, defValue);
            }
}