package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesciencias.adivinaanimales;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.itsjr.topicos.eaglelearning.R;

public class FragmetnCinco extends Fragment {


    Dialog dialogBien, dialogoMal;
    private boolean bandera = false;
    ImageButton mgallo, mvaca,mescuchar;
    MediaPlayer mpgallo, mpvaca, mperror, mpfelicidades;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cinco_adivina,container,false);


        dialogBien = new Dialog(getContext());
        dialogoMal = new Dialog(getContext());

        mgallo = view.findViewById(R.id.gallo_f5);
        mvaca = view.findViewById(R.id.vaca_f5);
        mescuchar = view.findViewById(R.id.id_escuchar_f5);
        mpgallo = MediaPlayer.create(getContext(),R.raw.gallo);
        mpvaca = MediaPlayer.create(getContext(),R.raw.vaca);
        mperror = MediaPlayer.create(getContext(),R.raw.error);
        mpfelicidades = MediaPlayer.create(getContext(),R.raw.felicidades);

        mescuchar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpgallo.start();
            }
        });

        mgallo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensajeCorrecto();
                mpfelicidades.start();
                bandera=true;
            }
        });

        mvaca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensajeError();
                mperror.start();
            }
        });
        return view;
    }

    public void mensajeError(){
        TextView txtcerrar;
        dialogoMal.setContentView(R.layout.mensaje_incorrecto);
        txtcerrar = dialogoMal.findViewById(R.id.id_cerrar_incorrecto);
        txtcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoMal.dismiss();
            }
        });
        dialogoMal.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoMal.show();

    }

    public void mensajeCorrecto(){

        TextView txtcerrar;
        dialogBien.setContentView(R.layout.mensaje_correcto);
        txtcerrar = dialogBien.findViewById(R.id.id_cerrar_correcto);
        txtcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBien.dismiss();
            }
        });
        dialogBien.getWindow().setBackgroundDrawable( new ColorDrawable(Color.TRANSPARENT));
        dialogBien.show();

    }
}
