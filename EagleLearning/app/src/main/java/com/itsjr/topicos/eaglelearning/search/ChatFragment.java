package com.itsjr.topicos.eaglelearning.search;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.databinding.FragmentChatBinding;

public class ChatFragment extends Fragment implements View.OnClickListener{

    FragmentChatBinding binding;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);

        binding.viewToolbar.tvTitle.setText("BIENVENIDO AL CHAT");

        setListeners();

        return binding.getRoot();
    }

    public void setListeners(){
        binding.btnChat.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_chat: startActivity(new Intent(getActivity(),ChatActivity.class));
                break;
        }

    }
}
