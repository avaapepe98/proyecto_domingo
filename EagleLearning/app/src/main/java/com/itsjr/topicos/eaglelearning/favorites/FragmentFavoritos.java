package com.itsjr.topicos.eaglelearning.favorites;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.databinding.FragmentFavoritosBinding;

/**
 * Created by Alexis on 31/03/2018.
 */

public class FragmentFavoritos extends Fragment {

    FragmentFavoritosBinding binding;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favoritos, container, false);

        binding.viewToolbar.tvTitle.setText("FAVORITOS");

        return binding.getRoot();
    }
}
