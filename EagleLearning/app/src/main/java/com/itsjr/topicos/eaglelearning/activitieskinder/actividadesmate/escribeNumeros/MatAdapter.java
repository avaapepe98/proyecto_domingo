package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesmate.escribeNumeros;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.webserver.services.actmat.getActMat;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MatAdapter extends RecyclerView.Adapter<MatAdapter.MatKinderViewHolder> {

    private final Context context;
    List<getActMat> contents;
    static final int TYPE_HEADER = 0;
    static MatAdapter.OnItemClickListener mItemClickListener;

    public MatAdapter(Context context, List<getActMat> contents) {
        this.context = context;
        this.contents = contents;
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_HEADER;
    }

    public void updateList(List<getActMat> data) {
        contents = data;
        notifyDataSetChanged();
    }

    public void addItem(int position, getActMat data) {
        contents.add(position, data);
        notifyItemInserted(position);
    }

    public void clear() {
        if (contents != null && contents.size() > 0) {
            contents.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public MatAdapter.MatKinderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        switch (viewType) {
            case TYPE_HEADER: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.card_view_esp_kinder, parent, false);
                return new MatAdapter.MatKinderViewHolder(view);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final MatAdapter.MatKinderViewHolder holder, final int position) {
        Picasso.with(context).load(R.mipmap.sin_foto).error(R.mipmap.sin_foto).into(holder.ivbanner);
        ViewCompat.setTransitionName(holder.ivbanner, "image");
        holder.tvtitle.setText(contents.get(position).getNombreAct());
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public static class MatKinderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvtitle;
        ImageView ivbanner;
        MatKinderViewHolder(View itemView) {
            super(itemView);
            tvtitle = itemView.findViewById(R.id.tv_title_act);
            ivbanner = itemView.findViewById(R.id.iv_banner_curso);
            ivbanner.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(ivbanner, getPosition());
            }
        }

    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final MatAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public getActMat getItem(int position) {
        return contents.get(position);
    }

}