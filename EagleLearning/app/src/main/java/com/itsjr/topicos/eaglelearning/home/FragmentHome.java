package com.itsjr.topicos.eaglelearning.home;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.activitieskinder.ActivityActividadesKinder;
import com.itsjr.topicos.eaglelearning.databinding.FragmentHomeBinding;
import com.itsjr.topicos.eaglelearning.webserver.Config;
import com.itsjr.topicos.eaglelearning.webserver.MySharedPreferences;
import com.itsjr.topicos.eaglelearning.webserver.RequestListener;
import com.itsjr.topicos.eaglelearning.webserver.RequestManager;
import com.itsjr.topicos.eaglelearning.webserver.services.cursos.RespCurso;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Response;

public class FragmentHome extends Fragment{

    FragmentHomeBinding binding;
    RespCurso nivel;
    AdapterCursos adapter;
    MySharedPreferences shared;

    SensorManager sensorManager;
    Sensor sensor;
    SensorEventListener sensorEventListener;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        binding.viewToolbar.tvTitle.setText("HOME");
        getNiveles();
        shared = new MySharedPreferences(getContext());

        //------------- sensor -------------

        return binding.getRoot();
    }

    public void setSensores(){
    }

    private void getNiveles() {
        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        RequestManager.getInstance().makeRequest(Config.URL_BASE + Config.ENDPOINT_GET_NIVEL_ACAD, Config.METHOD_GET, headers, params,null, new RequestListener() {
            @Override
            public void onResponse(Call call, Response response) {
                if (!isAdded()) {
                    return;
                }
                try {
                    String respuesta = response.body().string();
                    Log.i("TOPICOS", "response servicios" + respuesta);
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    nivel = mapper.readValue(respuesta, RespCurso.class);
                    FragmentHome.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            binding.progress.setVisibility(View.GONE);
                            initLista();
                        }
                    });
                } catch (IOException e) {
                    Log.w("EXCEPTION", e);
                }
            }
            @Override
            public void onError(Call call, IOException e) {
                FragmentHome.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Error al conectar con el servidor", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onTimeOut() {

            }
        });
    }

    private void initLista() {
        //binding.rvHome.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        binding.rvHome.setLayoutManager(new GridLayoutManager(this.getActivity(),2));
        adapter = new AdapterCursos(getActivity(), nivel.getNivel());
        binding.rvHome.setAdapter(adapter);
        binding.rvHome.setItemAnimator(new DefaultItemAnimator());
        adapter.setOnItemClickListener(onItemClickListener);
        adapter.updateList(nivel.getNivel());
    }

    AdapterCursos.OnItemClickListener onItemClickListener = new AdapterCursos.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position) {
            if(TextUtils.isEmpty(shared.getString("KEY_LOGIN","")))
                Toast.makeText(getActivity(),"Debes iniciar sesion",Toast.LENGTH_SHORT).show();
            else {
                Log.i("TOPICOS", "Click en " + nivel.getNivel().get(position).getNivel());

                if (nivel.getNivel().get(position).getNivel().equals("Preescolar")) {
                    Intent i = new Intent(getActivity(), ActivityActividadesKinder.class);
                    startActivity(i);
                } else
                    Toast.makeText(getContext(), "Aun No Disponile", Toast.LENGTH_SHORT).show();
            }
        }
    };
}
