package com.itsjr.topicos.eaglelearning.webserver;

public class Config {
    public static final int METHOD_GET = 0;
    public static final int METHOD_POST = 1;

    public static final int TIMEOUT = 30;
    public static final int MAX_REQUEST = 40;

    public static final String URL_BASE = "http://juguetes.servebeer.com/learningtogether/servicios/";

    public static final String ENDPOINT_GET_NIVEL_ACAD ="get_nivel_acad.php";
    public static final String ENDPOINT_GET_ACT_ESP_KINDER ="getActEspKinder.php";
    public static final String ENDPOINT_GET_ACT_MAT_KINDER ="getActMatKinder.php";
    public static final String ENDPOINT_REGISTER ="insertar_alumno.php";
    public static final String ENDPOINT_LOGIN ="obtener_alumno_por_id.php?contrasenia=";
    public static final String ENDPOINT_RECORD ="recordByPass.php?contrasenia=";
    public static final String ENDPOINT_ADD_RECORD ="insertRecord.php";

    public static final String NAME_SHARE_PREFERENCE ="name";
    public static final String NAME_SHARE_LOGIN ="login";
}