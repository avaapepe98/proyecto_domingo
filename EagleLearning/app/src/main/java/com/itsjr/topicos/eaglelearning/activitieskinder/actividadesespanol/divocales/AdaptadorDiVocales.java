package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesespanol.divocales;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class AdaptadorDiVocales extends FragmentPagerAdapter {
    private int num_items = 5;

    public AdaptadorDiVocales(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){

            case 0: return new FragmentA();
            case 1: return new FragmentE();
            case 2: return new FragmentI();
            case 3: return new FragmentO();
            case 4: return new FragmentU();
            default: return null;
        }
    }
    @Override
    public int getCount() {
        return num_items;
    }
}
