package com.itsjr.topicos.eaglelearning.webserver.services.record;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_record",
        "nombreAlum",
        "nombreAct",
        "contrasenia"
})
public class Record implements Serializable
{

    @JsonProperty("id_record")
    private String idRecord;
    @JsonProperty("nombreAlum")
    private String nombreAlum;
    @JsonProperty("nombreAct")
    private String nombreAct;
    @JsonProperty("contrasenia")
    private String contrasenia;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -8307116071284788205L;

    @JsonProperty("id_record")
    public String getIdRecord() {
        return idRecord;
    }

    @JsonProperty("id_record")
    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }

    @JsonProperty("nombreAlum")
    public String getNombreAlum() {
        return nombreAlum;
    }

    @JsonProperty("nombreAlum")
    public void setNombreAlum(String nombreAlum) {
        this.nombreAlum = nombreAlum;
    }

    @JsonProperty("nombreAct")
    public String getNombreAct() {
        return nombreAct;
    }

    @JsonProperty("nombreAct")
    public void setNombreAct(String nombreAct) {
        this.nombreAct = nombreAct;
    }

    @JsonProperty("contrasenia")
    public String getContrasenia() {
        return contrasenia;
    }

    @JsonProperty("contrasenia")
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}