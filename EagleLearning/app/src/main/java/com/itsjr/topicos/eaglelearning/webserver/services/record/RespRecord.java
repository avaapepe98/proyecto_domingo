package com.itsjr.topicos.eaglelearning.webserver.services.record;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "estado",
        "record"
})
public class RespRecord implements Serializable
{

    @JsonProperty("estado")
    private Integer estado;
    @JsonProperty("record")
    private Record record;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -4280084715779399790L;

    @JsonProperty("estado")
    public Integer getEstado() {
        return estado;
    }

    @JsonProperty("estado")
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @JsonProperty("record")
    public Record getRecord() {
        return record;
    }

    @JsonProperty("record")
    public void setRecord(Record record) {
        this.record = record;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
