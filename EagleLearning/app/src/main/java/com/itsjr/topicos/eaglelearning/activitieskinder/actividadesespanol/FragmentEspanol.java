package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesespanol;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.activitieskinder.AdapterActEspKinder;
import com.itsjr.topicos.eaglelearning.activitieskinder.actividadesespanol.divocales.DiVocalesActivity;
import com.itsjr.topicos.eaglelearning.databinding.FragmentEspanolBinding;
import com.itsjr.topicos.eaglelearning.webserver.Config;
import com.itsjr.topicos.eaglelearning.webserver.RequestListener;
import com.itsjr.topicos.eaglelearning.webserver.RequestManager;
import com.itsjr.topicos.eaglelearning.webserver.services.actividades.españolkinder.RespEspKinder;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Response;

public class FragmentEspanol extends Fragment {

    FragmentEspanolBinding binding;
    RespEspKinder espKinder;
    AdapterActEspKinder adapter;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_espanol, container, false);
        getActEspKinder();
        return binding.getRoot();
    }

    private void getActEspKinder() {
        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        RequestManager.getInstance().makeRequest(Config.URL_BASE + Config.ENDPOINT_GET_ACT_ESP_KINDER, Config.METHOD_GET, headers, params,null, new RequestListener() {
            @Override
            public void onResponse(Call call, Response response) {
                if (!isAdded()) {
                    return;
                }
                try {
                    String respuesta = response.body().string();
                    Log.i("TOPICOS", "response servicios" + respuesta);
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    espKinder = mapper.readValue(respuesta, RespEspKinder.class);
                    FragmentEspanol.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initLista();
                        }
                    });
                } catch (IOException e) {
                    Log.w("EXCEPTION", e);
                }
            }
            @Override
            public void onError(Call call, IOException e) {
                FragmentEspanol.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Error al conectar con el servidor", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onTimeOut() {

            }
        });
    }

    private void initLista() {
        binding.rvEspaKinder.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapter = new AdapterActEspKinder(getActivity(), espKinder.getActividades());
        binding.rvEspaKinder.setAdapter(adapter);
        binding.rvEspaKinder.setItemAnimator(new DefaultItemAnimator());
        adapter.setOnItemClickListener(onItemClickListener);
        adapter.updateList(espKinder.getActividades());
    }

    AdapterActEspKinder.OnItemClickListener onItemClickListener = new AdapterActEspKinder.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position) {
            Log.i("TOPICOS", "Click en " + espKinder.getActividades().get(position).getNombreAct());

            switch (espKinder.getActividades().get(position).getNombreAct()){

                case "¡Di vocales!": startActivity(new Intent(getActivity(), DiVocalesActivity.class));
                    break;
                case "¡Escribe Vocales!": Snackbar.make(view, "AUN  NO DISPONIBLE", Snackbar.LENGTH_LONG).show();
                    break;
                case "¡Coloca Vocales!": Toast.makeText(getActivity(), "AUN  NO DISPONIBLE", Toast.LENGTH_SHORT).show();
                    break;
                case "¡Completa vocales!": Toast.makeText(getActivity(), "AUN  NO DISPONIBLE", Toast.LENGTH_SHORT).show();
            }

        }
    };
}