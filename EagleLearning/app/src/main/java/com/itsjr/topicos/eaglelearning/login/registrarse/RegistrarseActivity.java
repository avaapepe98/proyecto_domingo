package com.itsjr.topicos.eaglelearning.login.registrarse;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.airbnb.lottie.L;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.databinding.ActivityRegistrarseBinding;
import com.itsjr.topicos.eaglelearning.webserver.Config;
import com.itsjr.topicos.eaglelearning.webserver.RequestListener;
import com.itsjr.topicos.eaglelearning.webserver.RequestManager;
import com.itsjr.topicos.eaglelearning.webserver.services.registro.RespRegistro;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Response;

public class RegistrarseActivity extends AppCompatActivity implements View.OnClickListener{

    ActivityRegistrarseBinding binding;
    RespRegistro reg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_registrarse);

        setListeners();

    }//onCreate

    public void setListeners(){
        binding.btnRegist.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_regist:
                if(isValidData()){
                    if(emailValidator(binding.etEmail.getText().toString())){
                        registro();
                    }
                }
                break;
        }
    }

    private JSONObject jsonRegistro(){

        JSONObject addUser = new JSONObject();

        try {
            addUser.put("usuario",binding.etUsuario.getText().toString());
            addUser.put("contrasenia",binding.etPasswordRepeat.getText().toString());
            addUser.put("nombre",binding.etNombre.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return addUser;
    }

    private void registro() {

        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        RequestManager.getInstance().makeRequest(Config.URL_BASE + Config.ENDPOINT_REGISTER, Config.METHOD_POST, headers, params, jsonRegistro(), new RequestListener() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    String respuesta = response.body().string();
                    Log.i("TOPICOS", "response " + respuesta);
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    reg = mapper.readValue(respuesta, RespRegistro.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getApplication(), reg.getMensaje(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Call call, IOException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplication(), "Error al conectar con la DB", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onTimeOut() {
            }
        });
    }

    private boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATNER = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATNER);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isValidData(){
        if (TextUtils.isEmpty(binding.etNombre.getText().toString())){
            binding.etNombre.setError("Ingresa tu nombre");
            return false;
        }
        if (TextUtils.isEmpty(binding.etUsuario.getText().toString())){
            binding.etUsuario.setError("Crea tu usuario");
            return false;
        }
        if (TextUtils.isEmpty(binding.etPassword.getText().toString())){
            binding.etPassword.setError("Crea una contraseña");
            return false;
        }
        if (!binding.etPasswordRepeat.getText().toString().equals(binding.etPassword.getText().toString())){
            binding.etPasswordRepeat.setError("Tienen que coincidir las contraseñas");
            return false;
        }
        return true;
    }
}