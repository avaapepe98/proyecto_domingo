package com.itsjr.topicos.eaglelearning.account;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.activitieskinder.actividadesespanol.FragmentEspanol;
import com.itsjr.topicos.eaglelearning.databinding.FragmentPerfilBinding;
import com.itsjr.topicos.eaglelearning.general.ActivityMain;
import com.itsjr.topicos.eaglelearning.login.registrarse.RegistrarseActivity;
import com.itsjr.topicos.eaglelearning.webserver.Config;
import com.itsjr.topicos.eaglelearning.webserver.MySharedPreferences;
import com.itsjr.topicos.eaglelearning.webserver.RequestListener;
import com.itsjr.topicos.eaglelearning.webserver.RequestManager;
import com.itsjr.topicos.eaglelearning.webserver.services.actividades.españolkinder.RespEspKinder;
import com.itsjr.topicos.eaglelearning.webserver.services.login.RespAlum;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Alexis on 31/03/2018.
 */

public class FragmentPerfil extends Fragment implements View.OnClickListener{

    FragmentPerfilBinding binding;
    RespAlum login;
    MySharedPreferences shared;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_perfil, container, false);
        binding.viewToolbar.tvTitle.setText("INICIAR SESION");

        setListeners();
        shared = new MySharedPreferences(getContext());

        return binding.getRoot();
    }

    public void setListeners(){
        binding.btnLogin.setOnClickListener(this);
        binding.idRegistrar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                if (isValidData())
                    login(binding.etPassLogin.getText().toString());
                break;
            case R.id.id_registrar:
                startActivity(new Intent(getActivity(),RegistrarseActivity.class));
                break;
        }
    }

    public boolean isValidData(){
        if(TextUtils.isEmpty(binding.etUsuarioLogin.getText().toString())){
            binding.etUsuarioLogin.setError("Ingresa tu usuario");
            return false;
        }
        if(TextUtils.isEmpty(binding.etPassLogin.getText().toString())){
            binding.etPassLogin.setError("Ingresa tu usuario");
            return false;
        }
        return true;
    }

    private void login(String contra) {
        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        RequestManager.getInstance().makeRequest(Config.URL_BASE + Config.ENDPOINT_LOGIN + contra, Config.METHOD_GET, headers, params,null, new RequestListener() {
            @Override
            public void onResponse(Call call, Response response) {
                if (!isAdded()) {
                    return;
                }
                try {
                    String respuesta = response.body().string();
                    Log.i("TOPICOS", "response servicios" + respuesta);
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    login = mapper.readValue(respuesta, RespAlum.class);
                    FragmentPerfil.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if(login.getEstado() == 1){
                                Toast.makeText(getActivity(),"Bien venido "+login.getAlumno().getNombre(), Toast.LENGTH_SHORT).show();
                                shared.putString("KEY_LOGIN",login.getAlumno().getNombre());
                                shared.putString("KEY_USER",login.getAlumno().getUsuario());
                                shared.putString("KEY_PASS",login.getAlumno().getContrasenia());
                                Log.i("NOMBRE", shared.getString("KEY_LOGIN",""));
                                Log.i("USUARIO", shared.getString("KEY_USER",""));
                                startActivity(new Intent(getActivity(), ActivityMain.class));
                            }
                            else
                                Toast.makeText(getActivity(),"Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();

                        }
                    });
                } catch (IOException e) {
                    Log.w("EXCEPTION", e);
                }
            }
            @Override
            public void onError(Call call, IOException e) {
                FragmentPerfil.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Error al conectar con el servidor", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onTimeOut() {

            }
        });
    }

}