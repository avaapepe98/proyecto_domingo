package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesmate.escribeNumeros.actividadesmate.ordenaNumeros;

import android.content.Context;
import android.net.Uri;
import com.itsjr.topicos.eaglelearning.R;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class OrdenaNumeros extends AppCompatActivity implements
        FragmentInstrucciones.OnFragmentInteractionListener,
        Fragment1_2.OnFragmentInteractionListener,
        Fragment3_4.OnFragmentInteractionListener,
        Fragment5_6.OnFragmentInteractionListener,
        Fragment7_8.OnFragmentInteractionListener,
        Fragment9_10.OnFragmentInteractionListener{

    //Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.ordena_numeros);

        //context = this;
        //setContentView(R.layout.activity_main);


        Fragment fragment = new FragmentInstrucciones();


        FragmentManager fragmentManager = getSupportFragmentManager();


        fragmentManager.beginTransaction().replace(R.id.contenedor, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

