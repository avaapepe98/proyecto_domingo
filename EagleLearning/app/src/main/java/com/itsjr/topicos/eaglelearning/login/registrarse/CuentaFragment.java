package com.itsjr.topicos.eaglelearning.login.registrarse;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.databinding.FragmentCuentaBinding;
import com.itsjr.topicos.eaglelearning.general.ActivityMain;
import com.itsjr.topicos.eaglelearning.webserver.MySharedPreferences;

public class CuentaFragment extends Fragment implements View.OnClickListener{

    FragmentCuentaBinding binding;
    MySharedPreferences shared;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cuenta, container, false);
        binding.viewToolbar.tvTitle.setText("MI PERFIL");

        shared = new MySharedPreferences(getContext());

        binding.etNombreUsuario.setText(shared.getString("KEY_LOGIN",""));
        binding.etUsuarioUsuario.setText(shared.getString("KEY_USER",""));

        setListeners();

        return binding.getRoot();
    }

    public void setListeners(){
        binding.btnLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_logout:
                logout();
                break;
        }
    }

    public void logout(){
        shared.putString("KEY_LOGIN","");
        shared.putString("KEY_USER","");
        startActivity(new Intent(getActivity(), ActivityMain.class));
    }
}
