package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesciencias.adivinaanimales;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.itsjr.topicos.eaglelearning.R;

public class FragmentCuatro extends Fragment {

    Button continuar;
    Dialog dialogBien, dialogoMal;
    private boolean bandera = false;
    ImageButton mgato, mperro,mescuchar;
    MediaPlayer mpgato, mpPerro, mperror, mpfelicidades;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_cuatro_adivina,container,false);

        continuar = view.findViewById(R.id.continuar_F4);

        dialogBien = new Dialog(getContext());
        dialogoMal = new Dialog(getContext());

        mgato = view.findViewById(R.id.gato_f4);
        mperro = view.findViewById(R.id.perro_f4);
        mescuchar = view.findViewById(R.id.id_escuchar_f4);
        mpgato = MediaPlayer.create(getContext(),R.raw.gato);
        mpPerro = MediaPlayer.create(getContext(),R.raw.perro);
        mperror = MediaPlayer.create(getContext(),R.raw.error);
        mpfelicidades = MediaPlayer.create(getContext(),R.raw.felicidades);

        mescuchar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpPerro.start();
            }
        });

        mperro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensajeCorrecto();
                mpfelicidades.start();
                bandera=true;
            }
        });

        mgato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensajeError();
                mperror.start();

            }
        });
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bandera==true){
                    Fragment fragment = new FragmetnCinco();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contenedor, fragment).addToBackStack(null).commit();
                }
                else
                {
                    Snackbar snackbar = Snackbar.make(view.findViewById(R.id.layout_cuatro),"Ops¡¡¡",Snackbar.LENGTH_SHORT)
                            .setAction("Reintentar", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mpPerro.start();
                                }
                            });
                    snackbar.show();
                }
            }
        });

        return view;
    }

    public void mensajeError(){
        TextView txtcerrar;
        dialogoMal.setContentView(R.layout.mensaje_incorrecto);
        txtcerrar = dialogoMal.findViewById(R.id.id_cerrar_incorrecto);
        txtcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoMal.dismiss();
            }
        });
        dialogoMal.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoMal.show();

    }

    public void mensajeCorrecto(){

        TextView txtcerrar;
        dialogBien.setContentView(R.layout.mensaje_correcto);
        txtcerrar = dialogBien.findViewById(R.id.id_cerrar_correcto);
        txtcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBien.dismiss();
            }
        });
        dialogBien.getWindow().setBackgroundDrawable( new ColorDrawable(Color.TRANSPARENT));
        dialogBien.show();

    }
}
