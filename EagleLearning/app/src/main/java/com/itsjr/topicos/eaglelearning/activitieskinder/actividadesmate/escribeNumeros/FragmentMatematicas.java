package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesmate.escribeNumeros;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.activitieskinder.AdapterActEspKinder;
import com.itsjr.topicos.eaglelearning.activitieskinder.actividadesespanol.divocales.DiVocalesActivity;
import com.itsjr.topicos.eaglelearning.activitieskinder.actividadesmate.escribeNumeros.actividadesmate.ordenaNumeros.OrdenaNumeros;
import com.itsjr.topicos.eaglelearning.databinding.FragmentEspanolBinding;
import com.itsjr.topicos.eaglelearning.databinding.MatematicasFragmentBinding;
import com.itsjr.topicos.eaglelearning.webserver.Config;
import com.itsjr.topicos.eaglelearning.webserver.RequestListener;
import com.itsjr.topicos.eaglelearning.webserver.RequestManager;
import com.itsjr.topicos.eaglelearning.webserver.services.actividades.españolkinder.RespEspKinder;
import com.itsjr.topicos.eaglelearning.webserver.services.actmat.RespMat;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Response;

public class FragmentMatematicas extends Fragment {

    MatematicasFragmentBinding binding;
    RespMat mat;
    MatAdapter adapter;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.matematicas_fragment, container, false);
        getActMatKinder();
        return binding.getRoot();
    }

    private void getActMatKinder() {
        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        RequestManager.getInstance().makeRequest(Config.URL_BASE + Config.ENDPOINT_GET_ACT_MAT_KINDER, Config.METHOD_GET, headers, params,null, new RequestListener() {
            @Override
            public void onResponse(Call call, Response response) {
                if (!isAdded()) {
                    return;
                }
                try {
                    String respuesta = response.body().string();
                    Log.i("TOPICOS", "response servicios" + respuesta);
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    mat = mapper.readValue(respuesta, RespMat.class);
                    FragmentMatematicas.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initLista();
                        }
                    });
                } catch (IOException e) {
                    Log.w("EXCEPTION", e);
                }
            }
            @Override
            public void onError(Call call, IOException e) {
                FragmentMatematicas.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Error al conectar con el servidor", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onTimeOut() {

            }
        });
    }

    private void initLista() {
        binding.rvMatKinder.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapter = new MatAdapter(getActivity(), mat.getActividades());
        binding.rvMatKinder.setAdapter(adapter);
        binding.rvMatKinder.setItemAnimator(new DefaultItemAnimator());
        adapter.setOnItemClickListener(onItemClickListener);
        adapter.updateList(mat.getActividades());
    }


    MatAdapter.OnItemClickListener onItemClickListener = new MatAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position) {
            Log.i("TOPICOS", "Click en " + mat.getActividades().get(position).getNombreAct());

            switch (mat.getActividades().get(position).getNombreAct()){

                case "¡Di número!": startActivity(new Intent(getActivity(), CompletaNumerosActivity.class));
                    break;
                case "¡Escribe el número!": Snackbar.make(view, "AUN  NO DISPONIBLE", Snackbar.LENGTH_LONG).show();
                    break;
                case "¡Coloca números!": startActivity(new Intent(getActivity(), OrdenaNumeros.class));
                    break;
                case "¡Completa números!": Toast.makeText(getActivity(), "AUN  NO DISPONIBLE", Toast.LENGTH_SHORT).show();
            }


        }
    };

}