package com.itsjr.topicos.eaglelearning.webserver.services.login;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "idAlum",
        "usuario",
        "contrasenia",
        "nombre"
})
public class Alumno implements Serializable
{

    @JsonProperty("idAlum")
    private String idAlum;
    @JsonProperty("usuario")
    private String usuario;
    @JsonProperty("contrasenia")
    private String contrasenia;
    @JsonProperty("nombre")
    private String nombre;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 9173001662545832841L;

    @JsonProperty("idAlum")
    public String getIdAlum() {
        return idAlum;
    }

    @JsonProperty("idAlum")
    public void setIdAlum(String idAlum) {
        this.idAlum = idAlum;
    }

    @JsonProperty("usuario")
    public String getUsuario() {
        return usuario;
    }

    @JsonProperty("usuario")
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @JsonProperty("contrasenia")
    public String getContrasenia() {
        return contrasenia;
    }

    @JsonProperty("contrasenia")
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    @JsonProperty("nombre")
    public String getNombre() {
        return nombre;
    }

    @JsonProperty("nombre")
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}