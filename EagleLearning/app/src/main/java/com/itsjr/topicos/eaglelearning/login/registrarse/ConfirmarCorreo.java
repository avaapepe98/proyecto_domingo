package com.itsjr.topicos.eaglelearning.login.registrarse;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.itsjr.topicos.eaglelearning.R;

public class ConfirmarCorreo extends AppCompatActivity {


    TextInputEditText codigo;
    Button confirmar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmar_correo);

        codigo = findViewById(R.id.codigo_verificar);
        confirmar = findViewById(R.id.btn_confirmar);

        confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(codigo.getText().toString().trim())){
                    codigo.setError("Codigo de verificacion");
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.layout_confirmar),"Escribe el codigo de verificacion",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
                else
                {
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.layout_confirmar),"En construcción...",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }

            }
        });

    }
}
