package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesespanol.divocales;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.itsjr.topicos.eaglelearning.R;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class FragmentE extends Fragment {

    Dialog dialogBien, dialogoMal;
    TextView vocal, verTexto;
    ImageButton microfono;
    MediaPlayer mediaPlayer1, mediaPlayer2;
    private String extraer_vocal, compara_vocal;

    int requestCode;
    int resultCode;
    Intent data;

    private static final int RECOGNIZE_SPEECH_ACTIVITY = 1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_e,container,false);

        dialogBien = new Dialog(getContext());
        dialogoMal = new Dialog(getContext());
        vocal = view.findViewById(R.id.vocal_e);
        verTexto = view.findViewById(R.id.vertexto_fe);
        microfono = view.findViewById(R.id.microfono_fe);
        mediaPlayer1 = MediaPlayer.create(getContext(),R.raw.felicidades);
        mediaPlayer2 = MediaPlayer.create(getContext(),R.raw.error);

        microfono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickImgBtnHablar();
            }
        });

        return view;
    }

    public void onActivityResult(int requestCode,int resultCode,Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case RECOGNIZE_SPEECH_ACTIVITY:
                if (resultCode == RESULT_OK && null != data){
                    ArrayList<String> speech = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String strSpeech2Text = speech.get(0);
                    verTexto.setText(strSpeech2Text);
                    revisarVocal();
                }
                break;

            default:

                break;
        }
    }
    public void onClickImgBtnHablar(){

        Log.i("click en","GRABAR");
        Intent intentActionRecognizeSpeech = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        // configurar a español de méxico
        intentActionRecognizeSpeech.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-MX");

        try {
            startActivityForResult(intentActionRecognizeSpeech,RECOGNIZE_SPEECH_ACTIVITY);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getContext(),"Tú dispositivo no soporta el reconocimiento por voz", Toast.LENGTH_SHORT).show();
        }

    }
    //---------------------------------------------------------------------------------------------

    public void revisarVocal(){

        extraer_vocal = verTexto.getText().toString();
        compara_vocal = vocal.getText().toString();

        if (extraer_vocal.equals(compara_vocal)){
            mensajeCorrecto();
            mediaPlayer1.start();

        }
        else {
            mensajeError();
            mediaPlayer2.start();
        }
    }

    public void mensajeError(){
        TextView txtcerrar;
        dialogoMal.setContentView(R.layout.mensaje_incorrecto);
        txtcerrar = dialogoMal.findViewById(R.id.id_cerrar_incorrecto);
        txtcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoMal.dismiss();
            }
        });
        dialogoMal.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoMal.show();

    }

    public void mensajeCorrecto(){
        TextView txtcerrar;
        dialogBien.setContentView(R.layout.mensaje_correcto);
        txtcerrar = dialogBien.findViewById(R.id.id_cerrar_correcto);
        txtcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBien.dismiss();
            }
        });
        dialogBien.getWindow().setBackgroundDrawable( new ColorDrawable(Color.TRANSPARENT));
        dialogBien.show();

    }
}
