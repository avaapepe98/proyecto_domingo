package com.itsjr.topicos.eaglelearning.favorites;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.activitieskinder.ActivityActividadesKinder;
import com.itsjr.topicos.eaglelearning.databinding.FragmentFavoritosLoginBinding;
import com.itsjr.topicos.eaglelearning.home.AdapterCursos;
import com.itsjr.topicos.eaglelearning.home.FragmentHome;
import com.itsjr.topicos.eaglelearning.webserver.Config;
import com.itsjr.topicos.eaglelearning.webserver.MySharedPreferences;
import com.itsjr.topicos.eaglelearning.webserver.RequestListener;
import com.itsjr.topicos.eaglelearning.webserver.RequestManager;
import com.itsjr.topicos.eaglelearning.webserver.services.cursos.RespCurso;
import com.itsjr.topicos.eaglelearning.webserver.services.record.Record;
import com.itsjr.topicos.eaglelearning.webserver.services.record.RespRecord;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class FavoritosLoginFragment extends Fragment {

    MySharedPreferences shared;
    FragmentFavoritosLoginBinding binding;
    RespRecord record;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favoritos_login, container, false);

        binding.viewToolbar.tvTitle.setText("ACTIVIDADES TERMINADAS");

        shared = new MySharedPreferences(getContext());

        getNiveles(shared.getString("KEY_PASS",""));

        return binding.getRoot();
    }

    private void getNiveles(String contra) {
        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        RequestManager.getInstance().makeRequest(Config.URL_BASE + Config.ENDPOINT_RECORD + contra, Config.METHOD_GET, headers, params,null, new RequestListener() {
            @Override
            public void onResponse(Call call, Response response) {
                if (!isAdded()) {
                    return;
                }
                try {
                    String respuesta = response.body().string();
                    Log.i("TOPICOS", "response servicios" + respuesta);
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    record = mapper.readValue(respuesta, RespRecord.class);
                    FavoritosLoginFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            binding.progress.setVisibility(View.GONE);
                            if (record.getEstado()==1)
                                initLista();
                            else
                                Toast.makeText(getActivity(), "No has completado ninguna actividad", Toast.LENGTH_SHORT).show();

                        }
                    });
                } catch (IOException e) {
                    Log.w("EXCEPTION", e);
                }
            }
            @Override
            public void onError(Call call, IOException e) {
                FavoritosLoginFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Error al conectar con el servidor", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onTimeOut() {

            }
        });
    }

    public void initLista(){
        binding.tvNomRecord.setText(record.getRecord().getNombreAlum());
        binding.tvActRecord.setText(record.getRecord().getNombreAct());
    }
}