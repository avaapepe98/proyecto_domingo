package com.itsjr.topicos.eaglelearning.webserver.services.actividades.españolkinder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "actividades"
})
public class RespEspKinder implements Serializable
{

    @JsonProperty("actividades")
    private List<Actividade> actividades = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -5949238337719462228L;

    @JsonProperty("actividades")
    public List<Actividade> getActividades() {
        return actividades;
    }

    @JsonProperty("actividades")
    public void setActividades(List<Actividade> actividades) {
        this.actividades = actividades;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}