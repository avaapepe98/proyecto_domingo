package com.itsjr.topicos.eaglelearning.general;


import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.itsjr.topicos.eaglelearning.R;

public class ActivitySplash extends AppCompatActivity{


    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    // llamar cuando el activity es creado
    Thread splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        iniciarAnim();
    }

    private void iniciarAnim() {
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.alpha);
        animation.reset();
        LinearLayout linearLayout = findViewById(R.id.lin_lay);
        linearLayout.clearAnimation();
        linearLayout.startAnimation(animation);

        animation = AnimationUtils.loadAnimation(this,R.anim.translate);
        animation.reset();
        ImageView imageView = findViewById(R.id.id_fondo);
        imageView.clearAnimation();
        imageView.startAnimation(animation);

        splash = new Thread(){
            public void run(){
                try{
                    int esperar = 0;
                    while (esperar<3500){
                        sleep(100);
                        esperar+=100;
                    }
                    Intent intent = new Intent(ActivitySplash.this, ActivityMain.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    ActivitySplash.this.finish();
                }catch (InterruptedException e){
                    // no hace nada
                }finally {
                    ActivitySplash.this.finish();
                }
            }
        };
        splash.start();
    }
}
