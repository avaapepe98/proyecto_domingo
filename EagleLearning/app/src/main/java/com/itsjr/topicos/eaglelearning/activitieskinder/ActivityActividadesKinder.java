package com.itsjr.topicos.eaglelearning.activitieskinder;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.activitieskinder.actividadesespanol.FragmentEspanol;
import com.itsjr.topicos.eaglelearning.activitieskinder.actividadesmate.escribeNumeros.FragmentMatematicas;
import com.itsjr.topicos.eaglelearning.databinding.ActivityActividadesKinderBinding;

public class ActivityActividadesKinder extends AppCompatActivity{

    ActivityActividadesKinderBinding binding;

    SensorManager sensorManager;
    Sensor sensor;
    SensorEventListener sensorEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_actividades_kinder);

        replaceFragment(R.id.frag_esp_kind,new FragmentEspanol());
        replaceFragment(R.id.frag_mat_kind,new FragmentMatematicas());
        setUpToolbar();

        //------------ sensores -------------------------
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor=sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (sensor == null){
            Toast.makeText(this,"No tienes este sensor",Toast.LENGTH_SHORT).show();
        }

        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.values[0]<sensor.getMaximumRange()){
                    getWindow().getDecorView().setBackgroundColor(Color.parseColor("#D1F2EB"));
                }
                else {
                    getWindow().getDecorView().setBackgroundColor(Color.parseColor("#FDFEFE"));
                }
            }
            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        start();

    }//onCreate

    public void start(){
        sensorManager.registerListener(sensorEventListener,sensor,10000000);
    }

    public void stop(){
        sensorManager.unregisterListener(sensorEventListener);
    }

    @Override
    protected void onPause() {
        stop();
        super.onPause();
    }

    @Override
    protected void onResume() {
        start();
        super.onResume();
    }

    //------------ fin sensor -------------------------------------

    private void setUpToolbar() {
        binding.viewToolbar.toolbar.setTitle("ACTIVIDADES");
        binding.viewToolbar.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        binding.viewToolbar.toolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
    }

    public void replaceFragment(int contentid, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(contentid, fragment)
                .commit();
    }

}
