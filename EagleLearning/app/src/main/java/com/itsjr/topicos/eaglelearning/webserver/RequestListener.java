package com.itsjr.topicos.eaglelearning.webserver;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

public interface RequestListener {

    public void onResponse(Call call, Response response);

    public void onError(Call call, IOException e);

    public void onTimeOut();

}
