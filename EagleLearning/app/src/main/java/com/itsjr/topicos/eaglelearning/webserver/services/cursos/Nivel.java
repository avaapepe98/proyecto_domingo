package com.itsjr.topicos.eaglelearning.webserver.services.cursos;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "idNivel",
        "nivel",
        "edad",
        "bannerNivel",
        "statusNivel"
})
public class Nivel {

    @JsonProperty("idNivel")
    private String idNivel;
    @JsonProperty("nivel")
    private String nivel;
    @JsonProperty("edad")
    private String edad;
    @JsonProperty("bannerNivel")
    private String bannerNivel;
    @JsonProperty("statusNivel")
    private String statusNivel;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("idNivel")
    public String getIdNivel() {
        return idNivel;
    }

    @JsonProperty("idNivel")
    public void setIdNivel(String idNivel) {
        this.idNivel = idNivel;
    }

    @JsonProperty("nivel")
    public String getNivel() {
        return nivel;
    }

    @JsonProperty("nivel")
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    @JsonProperty("edad")
    public String getEdad() {
        return edad;
    }

    @JsonProperty("edad")
    public void setEdad(String edad) {
        this.edad = edad;
    }

    @JsonProperty("bannerNivel")
    public String getBannerNivel() {
        return bannerNivel;
    }

    @JsonProperty("bannerNivel")
    public void setBannerNivel(String bannerNivel) {
        this.bannerNivel = bannerNivel;
    }

    @JsonProperty("statusNivel")
    public String getStatusNivel() {
        return statusNivel;
    }

    @JsonProperty("statusNivel")
    public void setStatusNivel(String statusNivel) {
        this.statusNivel = statusNivel;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}