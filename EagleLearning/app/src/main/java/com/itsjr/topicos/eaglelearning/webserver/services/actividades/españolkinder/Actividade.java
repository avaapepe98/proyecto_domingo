package com.itsjr.topicos.eaglelearning.webserver.services.actividades.españolkinder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "idActividad",
        "idNivel",
        "materia",
        "nombreAct",
        "bannerAct",
        "videoAct",
        "materialAct",
        "statusAct",
        "introduccion"
})
public class Actividade implements Serializable
{

    @JsonProperty("idActividad")
    private String idActividad;
    @JsonProperty("idNivel")
    private String idNivel;
    @JsonProperty("materia")
    private String materia;
    @JsonProperty("nombreAct")
    private String nombreAct;
    @JsonProperty("bannerAct")
    private String bannerAct;
    @JsonProperty("videoAct")
    private String videoAct;
    @JsonProperty("materialAct")
    private String materialAct;
    @JsonProperty("statusAct")
    private String statusAct;
    @JsonProperty("introduccion")
    private String introduccion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -5220289087203779395L;

    @JsonProperty("idActividad")
    public String getIdActividad() {
        return idActividad;
    }

    @JsonProperty("idActividad")
    public void setIdActividad(String idActividad) {
        this.idActividad = idActividad;
    }

    @JsonProperty("idNivel")
    public String getIdNivel() {
        return idNivel;
    }

    @JsonProperty("idNivel")
    public void setIdNivel(String idNivel) {
        this.idNivel = idNivel;
    }

    @JsonProperty("materia")
    public String getMateria() {
        return materia;
    }

    @JsonProperty("materia")
    public void setMateria(String materia) {
        this.materia = materia;
    }

    @JsonProperty("nombreAct")
    public String getNombreAct() {
        return nombreAct;
    }

    @JsonProperty("nombreAct")
    public void setNombreAct(String nombreAct) {
        this.nombreAct = nombreAct;
    }

    @JsonProperty("bannerAct")
    public String getBannerAct() {
        return bannerAct;
    }

    @JsonProperty("bannerAct")
    public void setBannerAct(String bannerAct) {
        this.bannerAct = bannerAct;
    }

    @JsonProperty("videoAct")
    public String getVideoAct() {
        return videoAct;
    }

    @JsonProperty("videoAct")
    public void setVideoAct(String videoAct) {
        this.videoAct = videoAct;
    }

    @JsonProperty("materialAct")
    public String getMaterialAct() {
        return materialAct;
    }

    @JsonProperty("materialAct")
    public void setMaterialAct(String materialAct) {
        this.materialAct = materialAct;
    }

    @JsonProperty("statusAct")
    public String getStatusAct() {
        return statusAct;
    }

    @JsonProperty("statusAct")
    public void setStatusAct(String statusAct) {
        this.statusAct = statusAct;
    }

    @JsonProperty("introduccion")
    public String getIntroduccion() {
        return introduccion;
    }

    @JsonProperty("introduccion")
    public void setIntroduccion(String introduccion) {
        this.introduccion = introduccion;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
