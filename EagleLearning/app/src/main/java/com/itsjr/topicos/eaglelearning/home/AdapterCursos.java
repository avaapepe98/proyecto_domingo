package com.itsjr.topicos.eaglelearning.home;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.webserver.services.cursos.Nivel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterCursos extends RecyclerView.Adapter<AdapterCursos.CursosViewHolder> {

    private final Context context;
    List<Nivel> contents;
    static final int TYPE_HEADER = 0;
    static OnItemClickListener mItemClickListener;

    public AdapterCursos(Context context, List<Nivel> contents) {
        this.context = context;
        this.contents = contents;
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_HEADER;
    }

    public void updateList(List<Nivel> data) {
        contents = data;
        notifyDataSetChanged();
    }

    public void addItem(int position, Nivel data) {
        contents.add(position, data);
        notifyItemInserted(position);
    }

    public void clear() {
        if (contents != null && contents.size() > 0) {
            contents.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public CursosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        switch (viewType) {
            case TYPE_HEADER: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.card_view_home, parent, false);
                return new CursosViewHolder(view);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final CursosViewHolder holder, final int position) {
        Picasso.with(context).load(contents.get(position).getBannerNivel()).error(R.mipmap.sin_foto).into(holder.ivbanner);
        ViewCompat.setTransitionName(holder.ivbanner, "image");
        holder.tvedad.setText(contents.get(position).getEdad());
        holder.tvnivel.setText(contents.get(position).getNivel());
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public static class CursosViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvedad, tvnivel;
        ImageView ivbanner;
        CursosViewHolder(View itemView) {
            super(itemView);
            tvedad = itemView.findViewById(R.id.tv_edad_curso);
            tvnivel = itemView.findViewById(R.id.tv_nivel_acad);
            ivbanner = itemView.findViewById(R.id.iv_banner_curso);
            ivbanner.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(ivbanner, getPosition());
            }
        }

    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public Nivel getItem(int position) {
        return contents.get(position);
    }

}