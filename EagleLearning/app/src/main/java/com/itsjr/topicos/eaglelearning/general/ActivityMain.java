package com.itsjr.topicos.eaglelearning.general;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;

import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.account.FragmentPerfil;
import com.itsjr.topicos.eaglelearning.databinding.ActivityMainBinding;
import com.itsjr.topicos.eaglelearning.favorites.FavoritosLoginFragment;
import com.itsjr.topicos.eaglelearning.favorites.FragmentFavoritos;
import com.itsjr.topicos.eaglelearning.home.FragmentHome;
import com.itsjr.topicos.eaglelearning.login.registrarse.CuentaFragment;
import com.itsjr.topicos.eaglelearning.search.ChatFragment;
import com.itsjr.topicos.eaglelearning.search.FragmentBuscar;
import com.itsjr.topicos.eaglelearning.webserver.MySharedPreferences;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class ActivityMain extends AppCompatActivity
    implements BottomNavigationViewEx.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener{

    ActivityMainBinding binding;
    MenuItem prevMenuItem;
    MySharedPreferences shared;

    //--- FRAGMENTS ---///
    FragmentHome fragmentHome;
    FragmentBuscar fragmentBuscar;
    FragmentFavoritos fragmentFavoritos;
    FragmentPerfil fragmentPerfil;
    CuentaFragment fragmentCuenta;
    FavoritosLoginFragment fragmentFavoritosLogin;
    ChatFragment fragmentChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setupBottomNavigationView();
        setListeners();

        shared = new MySharedPreferences(this);

        if (TextUtils.isEmpty(shared.getString("KEY_LOGIN","")))
            setupViewPager(binding.viewpager);
        else
            setupViewPagerLogin(binding.viewpager);

    }//onCreate

    public void setupBottomNavigationView(){
        binding.bottomNavigationEx.enableAnimation(false);
        binding.bottomNavigationEx.enableShiftingMode(false);
        //binding.bottomNavigationEx.enableItemShiftingMode(false);
        binding.bottomNavigationEx.setTextVisibility(false);
    }

    public void setListeners(){
        binding.bottomNavigationEx.setOnNavigationItemSelectedListener(this);
        binding.viewpager.addOnPageChangeListener(this);
    }//setListeners

    private void setupViewPager(ViewPager viewPager) {
    ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
    fragmentHome = new FragmentHome();
    fragmentBuscar = new FragmentBuscar();
    fragmentFavoritos = new FragmentFavoritos();
    fragmentPerfil = new FragmentPerfil();
        adapter.addFragment(fragmentHome);
        adapter.addFragment(fragmentBuscar);
        adapter.addFragment(fragmentFavoritos);
        adapter.addFragment(fragmentPerfil);
        viewPager.setAdapter(adapter);
    }//setupViewPager

    private void setupViewPagerLogin(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fragmentHome = new FragmentHome();
        fragmentChat = new ChatFragment();
        fragmentFavoritosLogin = new FavoritosLoginFragment();
        fragmentCuenta = new CuentaFragment();
        adapter.addFragment(fragmentHome);
        adapter.addFragment(fragmentChat);
        adapter.addFragment(fragmentFavoritosLogin);
        adapter.addFragment(fragmentCuenta);
        viewPager.setAdapter(adapter);
    }//setupViewPager

    //--- METODO PARA MANEJAR EL CAMBIO DE PANTALLA ORIENTADO A LOS "ITEMS" ---//
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                binding.viewpager.setCurrentItem(0);
                break;
            case R.id.search:
                binding.viewpager.setCurrentItem(1);
                break;
            case R.id.fav:
                binding.viewpager.setCurrentItem(2);
                break;
            case R.id.perfil:
                binding.viewpager.setCurrentItem(3);
                break;
        }
        return false;
    }

    //--- CREAS RESTRICCIONES PARA QUE SIEMPRE INICIE EN LA POSICION CERO DEL VIEW PAGER ---//
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (prevMenuItem != null)
            prevMenuItem.setChecked(false);
        else
            binding.bottomNavigationEx.getMenu().getItem(0).setChecked(false);
        binding.bottomNavigationEx.getMenu().getItem(position).setChecked(true);
        prevMenuItem = binding.bottomNavigationEx.getMenu().getItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}


    //------------------------------//
    @Override
    public void onBackPressed()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Salir?");
        builder.setCancelable(true);
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
