package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesmate.escribeNumeros;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itsjr.topicos.eaglelearning.R;

public class Fragment2 extends Fragment {


    LinearLayout contenedor;
    Boolean bandera = false;
    private final String numero = "dos";
    private EditText controles[];
    Button verificar, continuar;
    Dialog dialogBien, dialogoMal;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_2,container,false);


        dialogBien = new Dialog(getContext());
        dialogoMal = new Dialog(getContext());
        continuar = view.findViewById(R.id.btn_F2);


        contenedor = view.findViewById(R.id.layout_contenedorF2);
        controles = new EditText[numero.length()];
        verificar = view.findViewById(R.id.btn_verificarF2);

        for (int i = 0; i<controles.length; i++)
        {
            controles[i]= new EditText(getContext());
            controles[i].setTextColor(Color.BLACK);
            controles[i].setHint((i+1)+"    ");
            controles[i].setTextSize(20);
            controles[i].setHintTextColor(Color.parseColor("#FFC1CACE"));
            contenedor.addView(controles[i]);
        }

        verificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (notBlanck()){
                    if (getAllText().equals(numero)){
                        mensajeCorrecto();
                        bandera=true;
                    }
                    else
                    {
                        mensajeError();
                    }

                }
                else
                {
                    Snackbar snackbar = Snackbar.make(view.findViewById(R.id.F2_adivina_numeros),"Llena todos los campos",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });

        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bandera==true){
                    Fragment fragment = new Fragment3();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contenedor_adivina_numeros, fragment).addToBackStack(null).commit();
                }
                else
                {
                    Snackbar snackbar = Snackbar.make(view.findViewById(R.id.F2_adivina_numeros),"Ops¡¡¡",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });


        return view;
    }

    private String getAllText(){

        String respuesta = "";

        for (int i = 0; i<controles.length; i++)
        {
            respuesta = respuesta + controles[i].getText().toString();
        }


        return respuesta;
    }

    private boolean notBlanck(){
        boolean valor = true;

        for (int i = 0; i<controles.length; i++)
        {
            if (controles[i].getText().toString().isEmpty()){
                valor=false;
                break;
            }
        }

        return valor;
    }


    public void mensajeError(){
        TextView txtcerrar;
        dialogoMal.setContentView(R.layout.mensaje_incorrecto);
        txtcerrar = dialogoMal.findViewById(R.id.id_cerrar_incorrecto);
        txtcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoMal.dismiss();
            }
        });
        dialogoMal.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoMal.show();

    }

    public void mensajeCorrecto(){

        TextView txtcerrar;
        dialogBien.setContentView(R.layout.mensaje_correcto);
        txtcerrar = dialogBien.findViewById(R.id.id_cerrar_correcto);
        txtcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBien.dismiss();
            }
        });
        dialogBien.getWindow().setBackgroundDrawable( new ColorDrawable(Color.TRANSPARENT));
        dialogBien.show();

    }
}
