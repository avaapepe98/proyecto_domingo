package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesciencias.adivinaanimales;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.itsjr.topicos.eaglelearning.R;

public class ActivityAdivinaAnimales extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adivina_animales);

        context=this;
        setContentView(R.layout.activity_adivina_animales);

        Fragment fragment = new FragmentUno();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.contenedor, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
