package com.itsjr.topicos.eaglelearning.webserver.services.login;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "estado",
        "alumno"
})
public class RespAlum implements Serializable
{

    @JsonProperty("estado")
    private Integer estado;
    @JsonProperty("alumno")
    private Alumno alumno;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 1660714246850702817L;

    @JsonProperty("estado")
    public Integer getEstado() {
        return estado;
    }

    @JsonProperty("estado")
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @JsonProperty("alumno")
    public Alumno getAlumno() {
        return alumno;
    }

    @JsonProperty("alumno")
    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}