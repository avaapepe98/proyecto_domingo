package com.itsjr.topicos.eaglelearning.activitieskinder.actividadesespanol.divocales;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.itsjr.topicos.eaglelearning.R;
import com.itsjr.topicos.eaglelearning.databinding.ActivityDiVocalesBinding;

public class DiVocalesActivity extends AppCompatActivity {

    ActivityDiVocalesBinding binding;
    AdaptadorDiVocales adptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_di_vocales);
        adptador=new AdaptadorDiVocales(getSupportFragmentManager());
        replaceFragment(R.id.microfono_fa,new FragmentA());
        binding.pager.setAdapter(adptador);

    }//onCreate

    private void replaceFragment(int microfono_fa, FragmentA fragmentA) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(microfono_fa,fragmentA);
    }

}
