package com.itsjr.topicos.eaglelearning.webserver;

import android.util.Log;
import org.json.JSONObject;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RequestManager {

    //private static final int DATABASE_REQUEST_CODE = 20;
    private static RequestManager instance = null;
    private OkHttpClient client = null;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static RequestManager getInstance(){
        if (instance == null){
            instance = new RequestManager();
        }
        return instance;
    }

    public RequestManager() {
        client = new OkHttpClient.Builder()
                .connectTimeout(Config.TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(Config.TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Config.TIMEOUT, TimeUnit.SECONDS)
                .build();
        client.dispatcher().setMaxRequests(Config.MAX_REQUEST);
    }

    public void makeRequest(String url, int method, HashMap<String, String> headers, HashMap<String, String> params,
                            JSONObject json, final RequestListener listener){
        switch (method){
            case Config.METHOD_GET: {
                HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
                String urlFinal = urlBuilder.build().toString();
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    urlBuilder.addQueryParameter(entry.getKey(), entry.getValue());
                    urlFinal = urlBuilder.build().toString();
                }
                Request.Builder requestBuider = new Request.Builder();
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    requestBuider.addHeader(entry.getKey(), entry.getValue());
                }
                requestBuider.url(urlFinal);
                Log.i("TOPICOS", "URL GET " + urlFinal);
                Request request = requestBuider.build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.i("TOPICOS", "ERROR EN LA PETICION " + e.getMessage());
                        e.printStackTrace();
                        if (e instanceof SocketTimeoutException) {
                            Log.i("topicos", "TIME OUT ");
                            listener.onTimeOut();
                        } else
                            listener.onError(call, e);

                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if (!response.isSuccessful())
                            throw new IOException("Unexpected code " + response);
                        else
                            listener.onResponse(call, response);

                    }
                });
            }
            break;
            case Config.METHOD_POST: {
                Log.i("TOPICOS", "URL POST " + url);
                FormBody.Builder build = new FormBody.Builder();
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    Log.i("TOPICOS", "CON PARAMETROS " + entry.getKey() + " : " + entry.getValue());
                    build.add(entry.getKey(), entry.getValue());
                }
                RequestBody formBody = build.build();
                Request.Builder requestBuider = new Request.Builder();
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    requestBuider.addHeader(entry.getKey(), entry.getValue());
                }

                if (json != null)
                {
                    RequestBody body = RequestBody.create(JSON, json.toString());
                    requestBuider.post(body);
                }
                else
                    requestBuider.post(formBody);
                requestBuider.url(url);
                Request request = requestBuider.build();
                client.newCall(request).enqueue(new Callback() {

                    @Override
                    public void onFailure(Call call, IOException e) {
                        if (e instanceof SocketTimeoutException){
                            Log.i("TOPICOS", "TIME OUT ");
                            listener.onTimeOut();
                        }else
                            listener.onError(call, e);

                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if (!response.isSuccessful())
                            throw new IOException("Unexpected code " + response);
                         else
                            listener.onResponse(call, response);

                    }
                });
            }break;
        }

    }

    public void makeRequest(String url, int method, Map<String, String> headers, Map<String, String> params, JSONObject json, final RequestListener listener){
        Request.Builder requestBuider = new Request.Builder();

        for (Map.Entry<String, String> entry : headers.entrySet()) {
            requestBuider.addHeader(entry.getKey(), entry.getValue());
        }
        switch (method){
            case Config.METHOD_GET: {
                HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
                String urlFinal = urlBuilder.build().toString();
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    urlBuilder.addQueryParameter(entry.getKey(), entry.getValue());
                    urlFinal = urlBuilder.build().toString();
                }
                requestBuider.url(urlFinal);
                Request request = requestBuider.build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        if (e instanceof SocketTimeoutException)
                            listener.onTimeOut();
                        else
                            listener.onError(call, e);
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if (!response.isSuccessful())
                            throw new IOException("ERROR" + response);
                        else
                            listener.onResponse(call, response);
                    }
                });
            }
            break;
            case Config.METHOD_POST: {
                RequestBody body = RequestBody.create(JSON, json.toString());
                requestBuider.url(url);
                requestBuider.post(body);
                Request request = requestBuider.build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if (e instanceof SocketTimeoutException){
                            Log.i("TOPICOS", "TIME OUT ");
                            listener.onTimeOut();
                        }else
                            listener.onError(call, e);
                        e.printStackTrace();
                        listener.onError(call, e);
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        listener.onResponse(call, response);
                    }
                });
            }break;
        }

    }

    public OkHttpClient getClient() {
        return client;
    }

    public void setClient(OkHttpClient client) {
        this.client = client;
    }

}